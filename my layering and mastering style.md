# Layering

## What is a wall of sound?

Do you know what a wall of sound is? Before learning what that is, consider the following three elements, although for today (or now) we'll only focus on the wall of sound part.

Think of it like a pyramid where the foundation is especially important.

Thickness is the foundation 
The depth comes afterwards 
Then after that at the top of the pyramid, comes wall of sound

1) Thickness
2) Depth
3) Wall of sound

A wall of sound is like a huge thick swath of a sea of fog that is so pervasive and intrusive, due to the way everything is stacked together. 

For context, although Katy Perry and will.i.am songs typically have strong (bass) sounds and a crowded arrangement, that's STILL NOT the right type of layering to give their songs a wall of sound.

1) The chorus in the Robbie Williams Feel song
2) As the White Lies song progresses, the wall of sound gradually appears throughout the whole song
3) The chorus in the 3rd and 4th song

* [Robbie Williams - Feel](https://www.youtube.com/watch?v=iy4mXZN1Zzk)
* [My Dear Disco - White Lies](https://www.youtube.com/watch?v=5opS65No9ac)
* [Faithless - To All New Arrivals](https://www.youtube.com/watch?v=Qr7O_IMfHJQ)
* [Sonny J - Carabaret Short Circuit](https://www.youtube.com/watch?v=dSJXRL5baRs)

## Dense layering

The way I do layering is to make my music sound, to use just one word, dense. It has to be dense like stacked paper, not stacked bricks.

When I use soft bass sounds that are **weak**, emphasis on weak, while it's not a wall of sound (most of the time), there is a certain level of thickness to it, which seems surpising considering that I'm only using weak and thin elements. I mean, there are no strings, no sub bass (depending on the song) and no thick square bass.

In music production terminology, we call this a dense sound. But where does the dense sound come from? I really don't know. I can't answer that question. It even sounds dense before mastering the song and before putting any audio effects on any channel, bass or sample. Most of the time I don't even put any effects on the drums.

I suppose that when making music, that I just playback what I'm working on, feel that something is missing and then add something in, one or two things and then it becomes dense, even in the absense of thick (or strong) sounds being used as elements in the song.

My dense layering style, obscures and masks the  clarity of the individual elements, that would otherwise be noticeable if you had heard each element in isolation. It’s just my style.

You get a certain thickness that just comes out of nowhere, that wasn’t there originally in the weak and thin elements of sound originally - all without there being a wall of sound.

## Working from the skeleton outwards

One thing I do to try to keep it dense, is to work from the skeleton of the song outwards, instead of from left to right (towards the end of the song).

If I start out at the foundation of the song like a riff or the inner structure like a drum pattern, then work around it, from the inside moving slowly outwards, then it adds to that overall **dense** layering feeling.

## Pacing using a comparative repetitive pattern

Depending on what the song is, well especially if it's downtempo, drone music for museums or if it has downtempo elements, is that I'll use a percussive drum pattern or repetitive riff, to be present all throughout the entire song, to have **every** single element, fit that. 

Then once the song is done, just remove that element from the song, as it was never meant to be in the final song anyway. It was more like the Show Layer Guide and Show Pixel Grid feature in photoshop. 

_(I'll include an audio example once my laptop is fixed)_

## Playing 1 second and 1 bar snippets on loop

Have you ever walked into a room, that you wasn't aware had music playing before you walked in, to then enter at such a precise point throughout the song, that you've gotten the beginning and the end of every 4 bars (or stanza), completely wrong, as you had mis-judged when the stanza starts.

For example if the riff goes     `E-A-F-C-E-F-G-A`
then you misjudge where the stanza begins, so your brain thinks the riff is `F-G-A-E-A-F-C-E`, before it takes you a quick moment to mentally calibrate where the beginning of the riff and melody, actually is,

Well I want that effect on **all** my songs! My way to try to replicate that, is to keep playing back 1 second and 1 bar segments of my songs on loop. 

If that very short 1 second snippet resonates with me, enough to sway me over, then I'll keep where that part of the arrangement and its elements, exactly where it is, so I won't need to do any drastic changes. However if I don't like it and I keep on not liking it, them obviously I need to add something in or change things round. Well maybe not if I'm making a minimal song.

## Hiding subtle things when layering (for convergence to ensure tonational total arrangement)

### My diagram

![Three different light sources converging for diffused light then defracting](https://i.imgur.io/xWDmbhK_d.webp?maxwidth=640&shape=thumb&fidelity=medium)

Look at my diagram of how light moves around onwards from a fixed location. Do you remember being at school when the overhead bar lights were cheaper and low quality, so it was harder to read the textbook on your desk. And opening up the curtains by the window didn't help much, as it was winter.

For time's sake, imagine if I made my diagram bigger, by adding something at the top of it, where all those rectangles of light beams, start to break away from a shapely form to then go all wispy like spiderman spray webs, confetti or aerosol fumes. That's what I mean by how bad the lighting is for school kids with textbooks if the schools are using those shoddy cheap lights from 4-5 metres above.

### Taking a photo with good lighting

The face is a very peculiar and strange shape in nature, as there's parts of it that are more concave and some parts more convex, some tall and some wide, some wider at the top and some wider at the bottom.

Depending on how you take a photo of yourself, the shape and ratios of your face can look different. Angles and lighting!

What I like to do when taking photos of myself, is to avoid having any photos that have diffused light, as no matter how good the lens is for taking nature and architecture photos, most cameras are poor when it comes to diffused light, unless you're spending £2000 on a DSLR camera (not a £200-800 pocket camera).

But we can't exactly see properly which parts of the light are diffused, as we can't see the light beams. I just have to estimate and extrapolate.

But if all else fails, you can get 3 pieces of A4 paper, put a pen on them and see how the shadow forms as a comparison. Is the edge of the shadow sharp or blurred? Then you'll know what parts of the room to avoid standing in and where to place objects to block certain parts of the light.

### Why I don't listen to music most of the time

_(I've already explained this in the mesh discord server. Add it in anyway later for when it goes on my blog.)_

### How I try to replicate notational tonal variation

Remember when I said that I don't listen to music most of the time because I don't like how singular notes lack a tonal variation. If I put a microphone by my fridge to record it and slow it down by 800%, they'll be a tonal variation of singular notes.

So how can I try to replicate this effect, knowing the limitations of electronic music and computer sequenced music,

No I'm not talking or suggesting about merely changing the interpretation or infliction of the notes. So no, I'm not just talking about just changing the volume (or velocity) panning and equaliser of some of the notes, just to make it sound more live (like a live band) than pre-recorded. 

What I mean (and do), is to add very subtle changes to the elements, based on what other elements are playing on top of it, at the same time. The listener isn't supposed to notice them, so it's beyond subtle, it's inconspicuous.

_(I'll add audio examples once my laptop is fixed, as then I can open my song project files from years ago. Fill this in later.)_

1. If there is distortion being used, make it slightly different if the sound subject of the distortion effect, is hit on a minor key (D or A) to be **slightly** than it would on a minor key 
2. Quickly changing the panning of an element to the left then back to normal in a subtle way
3. Add extra notes in within a repetitive element, that is designed to blend in and feel like the same note as before. This works well on very low bass sounds.
4. If there are 1000 cents within the note C and D, then temporarily change the microtuning of a note by minus 200 (-200) before changing it back to normal
5. Temporarily make the drums more vivid for a few seconds using an equaliser, compression, high pass filter and a chorus, before putting it back to normal. 
6. After somewhere halfway through the song, make the notes stutter by having a very short delay that ends prematurely but in a way that is beyond subtle, inconspicuous and hardly noticible.
7. Have the volume of one atmospheric inducing element, constantly fluctuate up and down at a quick pace, that's hardly noticable.
8. Reduce the sound quality of an instrument or element, however you do so, when it's being overlaid with lots of other ones, then bring it back to its original high quality once it's less crowded with a more minimal arrangement
9. _I'll fill this in once my laptop is fixed_
10. _I'll fill this in once my laptop is fixed_

# Mastering
![Multiband Compressor from FL Studio](https://i.imgur.io/5gLLnTH_d.webp?maxwidth=640&shape=thumb&fidelity=medium)

Multiband Compressor from FL Studio

![Fruity Free Filter from FL Studio](https://i.imgur.io/QaBe6Bu_d.webp?maxwidth=640&shape=thumb&fidelity=medium)

Fruity Free Filter from FL Studio

![Maximus from Image Line](https://i.imgur.io/cYICrCq_d.webp?maxwidth=640&shape=thumb&fidelity=medium)

Maximus from FL Studio

![Fruity Panoramic EQ 2 from FL Studio](https://i.imgur.io/Km0Jsov_d.webp?maxwidth=640&shape=thumb&fidelity=medium)

Fruity Panoramic EQ from FL Studio

![What is JPEG artefacts 1](https://i.imgur.io/iyn5Tpl_d.webp?maxwidth=640&shape=thumb&fidelity=medium)

![What is JPEG artefacts 2](https://i.imgur.io/un8Cnkb_d.webp?maxwidth=640&shape=thumb&fidelity=medium)

![What is jpeg artefacts 3](https://i.imgur.io/XSzPvxS_d.webp?maxwidth=640&shape=thumb&fidelity=medium)

My mastering style tends to be

* **Muddy** (elements just have a way of expanding outside their natural frequency band, whether low/mid/high)
* **Raw** (the opposite of clean) [[example of clean song 1]](https://youtu.be/DK_0jXPuIr0) [[example 2]](https://youtu.be/P-_g4zYecUs) 
* **Jagged edges** (let's keep the jpeg-esque artefacts in the sound)
* **Hands-off** (not to enhance or refine the sounds but instead it's to do the least as possible and take away the least as possible. Most likely it'll be to turn down the treble on the drums or hihats a tiny bit.)
* **Dense** (I've covered this earlier but for mastering, the way that it's kept dense, it obscures the rich vivid nature of the elements in the track, that are better noticed when played in isolation. I've explained what I mean by dense, earlier on in the article.)

## Mastering styles I don't like

* Compressing everything down on a vocal dubstep song, to leave more space in the wave to increase the bass drop [deadmau5's opinion](https://youtu.be/zH_5Ug2iOLU) [Nero - Promises](https://youtu.be/llDikI2hTtk)
* [Dangerdisko - Hedonism album](https://dangerdisko.bandcamp.com/album/hedonism)
* [Oliver Dollar - Another Day Another Dollar](https://classicmusiccompany.bandcamp.com/album/another-day-another-dollar)
* Loudness War inspired mid-range albums, especially on remastered. I've already given comparative song examples in the discord server. [loudness war video 1](https://youtu.be/3Gmex_4hreQ) [video 2](https://youtu.be/0EIRF7QW0eA)
* Tom Snow before I gave him advice [about his song](https://youtu.be/TNTQ-WknFGM) on request about keeping the jpeg-risqué artefacts

# Introduction

I'll polish this up with a proper introduction, conclusion and whatnot, once it's posted onto my blog.

# The antiquated hardware of the past


Back in the olden days, before I was born, music was made on hardware because computers were so expensive at the time, that it was cheaper to get it done on hardware. I don't know how much it cost back then (the music production hardware) but when I was a child in 1998, I was 6 and my mum spent £1000 on a desktop computer with 7.5GB space, 500MB RAM, Intel Pentium 3. As you can imagine, it was slow.

Imagine you've got the synthesizer keyboard, drum machine, sequencer, mixer and a rack.

![arturia rack](https://i.imgur.com/zLlygMY_d.webp?maxwidth=1520&fidelity=grand)

![arturia mixer](https://i.imgur.com/SOD2FnH_d.webp?maxwidth=760&fidelity=grand)

![arturia drum machine](https://i.imgur.com/W6g92MX_d.webp?maxwidth=760&fidelity=grand)
 
![arturia keyboard controller](https://i.imgur.com/djkM6Sx_d.webp?maxwidth=760&fidelity=grand)

![ox instruments drum machine](https://i.imgur.com/pyU9gHm.jpg)

The problem was that (before I was born)

1) You could only have 8 tracks on your sequencer at the same time
2) Each sample could only be 30 seconds
3) There was only 64MB or 80MB of RAM

If you wanted to have a 9th track then you had to record over an existing track (this was called an overdub). But that would reduce the quality so some people would clone their source material onto cassettes, a Zip drive or microcassettes.

To record vocals, you had to have the person sing stuff multiple times or only one part at a time, to then stitch it together.

The biggest problem was the 64MB or 80MB of RAM. So you couldn't have much processing power to have lots of synthesizers/effects/elements playing at once.
Or maybe just the 8-track sequencers. The hardware was colloquially given the name 8-track.

Because of this people had to be MORE INTUITIVE AND CRAFTY with the type of music they make as they were more technological CONSTRAINTS, so they had to push the boundaries a bit more - well compared to today's younger producers (zoomers) anyway.

# #1 It’s 8-Track Centric

Here's some examples of some old music that's (relatively more) minimal and sounds very 8-track. 

These songs really do sound like they were made of the technology of the past, 8 track playing at once, each sample being 30 seconds maximum or 40MB or 100MB of RAM (memory).

## Loved Examples

[Origin of the shadows - Valley Unknown](https://www.youtube.com/watch?v=a5meT63flnM)

[Deep City - Lets Dance (Retro Vocal Remix)](https://www.youtube.com/watch?v=qUrYQYpcsLoy)

[Steve Mac & Yousef - Darkness 01](https://www.youtube.com/watch?v=FKMO-3GE5_M)

[Nat Monday - Waiting (Creamer and Stephanie K Remix)](https://www.youtube.com/watch?v=EOGmMHlRebc)

[FC/Kahuna - Mind Set To Cycle](https://www.youtube.com/watch?v=wtoKSoS1i5k)

[Felix da Housecat - Madame Hollywood (Extended)](https://www.youtube.com/watch?v=iO18OvBjOrA)

[Krafty Kuts - The Funk Is](https://www.youtube.com/watch?v=adzq8GFiOrQ)

[Todd Edwards - Mystery (Extended Edit)](https://youtu.be/fyUepS5xjwY)

[The Bucketheads - These Songs Fall Into My Mind (Armand Van Helden Edit)](https://youtu.be/V8iqZMEwDfk)

[Saeed and Palash - Watching You](https://www.youtube.com/watch?v=16rylwiSa-U)

[We're only science](https://www.youtube.com/watch?v=_T_jVP9JCtA)

[Rollo Goes Mystic - Love Love Here I Come](https://www.youtube.com/watch?v=NPY1MoDE3LU)


## Hated Example

[Martin Garrix - Proxy](https://www.youtube.com/watch?v=NWB6-PJw4Mk)

[Wild Girl - Paul Woolford Remix](https://www.youtube.com/watch?v=nde0wa27fJs)

[Krystal Klear - Neutron Dance](https://www.youtube.com/watch?v=oRVeF5KPngE)

[Chvrches - Good Girls](https://youtu.be/du4kNAyjVCg)

[Piri and Tommy - On and On](https://youtu.be/P0_S9JJ16fY)

[Matrix & Futurebound - Magnetic Eyes](https://youtu.be/SPSlVmRGlus)

[Imany- Don’t Be Shy (Filatov & Karas Remix)](https://youtu.be/b1_B-IKEufg)

[Rudimental - Feel The Love](https://youtu.be/oABEGc8Dus0)


# #2 It’s Cassette Beat-tape Centric

In the olden days for DJ's, before it became completely digital with the help of laptops, people would create a casette beat tape using a hi-fi. If you're too young to know what that is, I'll explain.

![hifi setup 1](https://i.imgur.io/uTnnrdp_d.webp?maxwidth=640&shape=thumb&fidelity=medium)
![hifi setup 2 photo 1](https://i.imgur.io/WPxd6WI_d.webp?maxwidth=640&shape=thumb&fidelity=medium)
![hifi setup 2 photo 2](https://i.imgur.io/JTvfVLR_d.webp?maxwidth=640&shape=thumb&fidelity=medium)
![hifi setup 2 photo 3](https://i.imgur.io/wAEqJLc_d.webp?maxwidth=640&shape=thumb&fidelity=medium)

People would have a blank casette for recording DIY makeshift instrumentals that were taken from oher songs, to be recorded from the already-written casette to the blamk casette. However as these people were too poor to afford the proper hardware equipment for making music, they were forced to use a hi-fl to make a makeshift DIY instrumental. 

People woukd record the Top 40 Chart ot whatever rlse onto a blank cassette, to them use different rack devices from the hi-fi, like equaliser, reverb, to try to strip out the vocals and isolate certain elements from the song. 

These casette beat tapes would be used at live shows and the cassette packaging would have a timestamped ttacklisting of what type of beat is where. Femember that as these DJ's could not afford the more expensive music production equipment for making casette beat tapes, they had to use hi-fi instead that had lots of racks on it. 

They would have singers and rappers over the makeshift beats as well as overlaying them on top of other songs on vinyl. Also some hi-fi devices would have 4 casette players in it, not 2, which made it easier to makeshift copy elements from multiple songs, to be overlaid (overdub) onto the same casette tape.

## Loved examples

* [Avicii - Levels (Otto Knows Remix) / mashup with ATB - Twisted Love](https://soundcloud.com/flukemachine/avicii-vs-atb-feat-cristina)
* [Avril Lavinge - Bite Me](https://youtu.be/ciqUEV9F0OY) 
* [Ashley Tisdale - Looking Glass ](https://youtu.be/H8ZgB5de-0I)
*  [Chromeo - Jealous - Funk LeBlanc Remix](https://youtu.be/1Dk8MmV_c7Q)
* [KDA - Turn The Music Louder](https://youtu.be/vh44zE-hlL4)
* [Britney Spears - Piece of Me](https://youtu.be/u4FF6MpcsRw)
* [Demi Lovato - Quiet](https://youtu.be/EplihNVv-Yw) 
* [Akon - Rely On](https://soundcloud.com/user-36629235-776459904/akon-rely-on)

**As you could not afford a £2000 hardware for music production (eg. Yamaha, Korg, Casio) and that your software on Windows 98 and Intel Pentium 3, it was too slow to even bother, so therefore you was VERY RESTRICTED with what you could do with your £500 hi-fi with its limited amount of devices and the knobs, buttons and dials**

## Hated examples 

* [Skrillex - summit](https://youtu.be/-e_3Cg9GZFU)
* [Halsey - I'm Not Mad](https://youtu.be/OCkohjacFU0)
* [Halsey - Wipe Your Tears](https://youtu.be/5PXQB9hOLCw)
* [Dua Lipa - Love Again](https://youtu.be/BC19kwABFwc)
* [Naughty Boy - la la la ](https://youtu.be/3O1_3zBUKM8)
* [Bebe Rexha - I'm a Mess](https://youtu.be/LdH7aFjDzjI)
* [As it is - the stigma](https://youtu.be/SxV1Jwg9xCk)
* [Marshmello - Here With Me](https://youtu.be/J3UXp9jIr-U)

# #3 It’s Cassette Cue-point jumping centric

Another thing that they would do is have the cassette burned onto CD to have an expensive CDJ machine that allowed mixing with vinyl but with no silences between the tracks or while seeking. They would write on the paper CD imserr, the timestamps of what beat is where. 

And unlike the cassette, switching to CD allowed them to setup cue points, so they could instantly jump from one time to another. There was even a blank vinyl which could simulate the scratching and analogue rewind of the CD in the CDJ machine. Also the DJ could just press different buttons on an electronic drum machine (an MPC) and they could INSTANTLY switch to different cue points and it STILL sound good.

## Loved example

* [will.I.am - Hey You](https://youtu.be/XXp2GxEbAN0)
* [Halsey - Alone (Calvin Harris Remix)](https://youtu.be/mlEYDB5Z59w)
* [Lil Wayne - Ready For The World](https://youtu.be/uyD6CZTWxpk)
* [Charli XCX - 5 in the morning](https://youtu.be/h-l-rcS0Awc)
* [Billy Talent - Saint Veronika](https://youtu.be/XYr5IC-mGi4)
* [Akon - Rely On](https://soundcloud.com/user-36629235-776459904/akon-rely-on)
* [The Used - Take It Away](https://youtu.be/zntsH1ceN5I)

## Hated examples
*  [Example - Perfect Replacement (R3hab Remix)](https://youtu.be/0HQDf__kCp4)
* [Can You Feel It (Redfoo Remix)](https://soundcloud.com/redfoo/can-you-feel-it-redfoo-bootleg)
* [Vault - Wheelchair](https://youtu.be/UhjkmtuIz2Y)
*  [Ariana Grande - God Is a Woman](https://youtu.be/kHLHSlExFis)
* [Simple Plan - Congratulations](https://youtu.be/QMLx5eN_3UU)
* [Marshmello - Here With Me](https://youtu.be/J3UXp9jIr-U)
* [Simple Plan - I Won't Be There](https://youtu.be/lnlMbmpHlgQ)

# #4 Timestretch centric

Some songs sound good when you speed them up and slow them down - and some songs don't. 

## Loved examples
* [Britney Spears - Now That I've Found You](https://youtu.be/e4_hkDXX7nk)
* [Britney Spears - Chillin With You](https://youtu.be/o6c4VDr1yC4)
* [T-Pain - 5 o clock ](https://youtu.be/noLrCDzAp5M) (in fact every t-pain song)
* [Ashley Tisdale - Looking Glass](https://youtu.be/H8ZgB5de-0I)
* [Lil Wayne- Shimmy](https://youtu.be/hQGcmVGt0cc)
* [The Used - Paralysed](https://youtu.be/rD-bOewdQts)
* [Billy Talent - Diamond on a Goldmine](https://youtu.be/UqITR9QrdcI)
* [Skint & Demoralised - Maria's Fall From Grace](https://youtu.be/dsoJdtG5ZZU)

## Hated examples  
* [Avicii - Wake Me Up](https://youtu.be/IcrbM1l_BoI)
* [Halsey - Wipe Your Tears](https://youtu.be/5PXQB9hOLCw)
* [Iggy Azalea - Fancy](https://youtu.be/O-zpOMYRi0w)
* [All American Rejects - Dirty Little Secret](https://youtu.be/gPDcwjJ8pLg)
* [Her Bright Skies - Bored](https://youtu.be/khLM7z610dY)
* [Twenty One Pilots - Guns For Hands](https://youtu.be/Pmv8aQKO6k0)

# #5 Removing the 2nd bar of every stanza, still keeps the arrangement sound seemingly okay

![example of time signature on fl studio](https://i.imgur.com/0I4n49n.jpg)

* If there's 4 beats in a bar (and 4 steps in a beat)
* 4 bars in a stanza
* 4 stanzas in a verse

Some songs sound off-key or out of place if you remove the second bar from every stanza

## Hated examples

* Olivia Rodrigo - Sour Album
* The xx - the first 2 albums
* [Dancing on my own from Rosaline soundtrack](https://youtu.be/5Gv458blOms)

# Conclusion
for the blog

# The music is made for compilations, not an album or even an EP

_I covered that already in the Mesh discord server._
_Later fill it in, when it goes on my blog._

# They focus more on the conventions than the conceptual aspect

From my personal experience, record labels don't really care if the songs in the demo are unfinished or ending prematurely because they're looking more at a person's conceptual understanding of music rather than the conventional side.

I think that the most common mistake musicians make when sending demos, is not "blasting" the demo by sending it to 100+ record labels without seeing whether it suits the label beforehand. But instead that they place too much empahsis on the conventions of the music rather than the conceptual side.

I don't know what sort of demos that labels receive but I would imagine that for the VAST majority of them, maybe around 80%, if you changed the bass sounds of every sound by switching to a different synthesizer preset (or even the one from the previous track), that it would sound the same as the previous song, like the same person made it.
```
You're using the same kind of notes in the same type of piano pattern, you've just changed the sounds round.
```
I think when sending demos, the best advice is to have songs that don't sound like the same person made it (1) and doesn't sound like you've just switched the bass sounds round (2). Prioritise the concepts over the conventions (3).

I would estimate that most of the people sending demos, fail to show these 3 aspects. You just don't want it to sound like "painting/colour by numbers" or cliché.

What a musician should do when sending demos, is make their song BLEND IN whilst being interspersed amongst 10 songs on the RADIO of the SAME genre, to sound like someone ELSE made it and not you, all whilst STILL sounding original and unique

Contradictory huh? If that makes any sense?

For if you're still confused, try making a demo of 12 songs of 3-5 minutes, **they don't have to be finished and you don't have to be swayed over** to fully like them. Then make one song of the same genre as the 10 radio songs, all of the same genre, then show your friends your 12 song demo, 10 song radio playlist, then ask them whether your ONE other song would be convincing enough to them that they made it, not you, if they'd never heard of you.

#  4/3 Polyrhythm

In the olden days, every song was a waltz at the 3/3 time signature. It only changed to 4/4 because it was easier to dance to. And they sometimes broke the waltz rules by making it 3/4 instead of 3/3 because it was easier for instrumentalists to know when one bar or stanza had ended, to then move onto the next one, especially if there's lots of people playing at the same time.

Me personally as a piano player who can read sheet music, I prefer to use the 4/3 time signature in my songs. If there's 4 beats in a bar and 4 bars in a stanza, that's common time, the most common time signature. If the song has lyrics then the verse section will probably be 4 verses, each with 4 lines. Well what I like to do is to instead have 4 beats in a bar yet 3 bars in a stanza (a stanza is a line that belongs to a verse). That's 4/3

But it can't be blatantly obvious!!!!

![Polyrhythm example 1](https://i.imgur.io/poHdkzq_d.webp?maxwidth=640&shape=thumb&fidelity=medium)

![Polyrhythm example 2](https://i.imgur.io/F5LwT5h_d.webp?maxwidth=640&shape=thumb&fidelity=medium)

I think when it comes to dance music or uptempo music that is hypnotic or jumpy, a strict 3/3 or 4/3 time signature, it just doesn't work, as it's hard to dance to and hard to become ensnared by it's hypnotic nature. So I would have a 4/4 time signature but there would be a POLYRHYTHM that's 4/3 so it overlays the 4/4 rhythm but it'll be done in a subtle way, not an obvious way. But when making downtempo music, then I probably will use the 4/3 time signature or a middle ground of 5/4 (just rarely) to get the best of both worlds.

I just think that the 4/3 sounds better. 🙂
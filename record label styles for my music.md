# Label Styles

I would say that my style is 
* 70% hyperdub
* 15% Kindisch
* 15% Depthcore

## Hyperdub

It's supposed to feel like you've just licked a 9V battery to be intermittently dejected from your current frame and place in time, for a few seconds

Or something like being at the theme park on the 100 metre drop, you’re all the way at the top, until you get the feeling of it falling down for a few seconds

Or seeing the bus on the other side of the road, the last bus before being late. But the traffic light at the zebra crossing isn’t green yet, so you decide to run in front of a dual carriageway where the cars are moving at 5mph, you try to dodge the cars and get hit by one, your shoe falls off but you’re not injured. Then you manage to catch the bus to successfully avoid being late.

[spotify link for 5 Years Of Hyperdub](https://open.spotify.com/album/2ChEUr5szB2CiR8cSOSApC?si=7cdmMXqFS1SFs5LH2u2m7Q)

## Depthcore

And there’s that Depthcore Records type sound, whether it’s on that label or not.
Look at the type of artwork that Depthcore has on their website.
It’s larger than life and it’s supposed to jump through the page,
If it was on a billboard or was flyposted, you can’t not notice it when you walk past it,
The Depthcore emotion is to imagine you’re making music for a tv advert where there’s vivid colour shooting out the tv. Remember it’s not to be as understated and meek as a jewellery advert.

[depthcore website](http://Depthcore.com)

[depthcore soundcloud account](https://soundcloud.com/depthcoremusic)

[sony Bravia advert 1](https://youtu.be/0_bx8bnCoiU)

[sony Bravia advert 2](https://youtu.be/q-ut_2GWIm4)

## Kindisch

Kindisch is an organic house record label and I think the best way to understand its sound, is to listen to other organic house labels

* All Day I Dream - A cross between Hed Kandi and scenic music while filming down below from a hot air balloon
* Akbal Music - meditation and yoga music
* Anjunadeep - The arrangement is suited for the elements being swapped with strings, as a string underbed, constant plucking or string melody
* Shanti Radio - It sounds like a DJ Tool. A DJ Tool is not really a song, it's just designed to be mixed over to have another song playing on top of it, except that for Shanti Radio, it's supposed to be enjoyable for the music consumer as well.

Kindisch just sounds gritty and that it takes the concept of cue-point jumping, to the extreme.

Listen to these

* Kindisch Steps II
* Kindisch 2017
* Get Physical Sessions - Selected Tracks Part 6

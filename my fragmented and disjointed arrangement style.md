# My fragmented and disjointed arrangement style

For someone who plays the piano, people expect if I was to make music that is melodic, that it would be melodic in a VERY linear sequential way. But that's not my style.

Use these 2 songs as a reference, to compare to.

## Archetypal example to compare against

[Audiocrunch - Go To School Kid](https://youtu.be/tUJvxdKKe7k)

My brother made this song, the Strive song. He likes rock music and his favourite record label is Fuelled By Ramen

[Junkk - Strive](https://soundcloud.com/junkk/strive)

# Shifting Melodic Forefront

Compare it to this unfinished WIP (work in progress) song that I'm not swayed over with. I just don't like it. 

I think the only way to improve it is to make a VIP (variation in production) version but then it would be a completely different song.

[You're Not Finished WIP (no drums no drop](https://soundcloud.com/tynamite/tynamite-youre-not-serious-no/s-yeEeJYcWZ9n)

Look at this watercolour landscape painting by famous illustrator Simon Ritter.

![watercolour painting by Simon Ritter](https://i.imgur.io/ThfxsRt_d.webp?maxwidth=640&shape=thumb&fidelity=medium)

Notice how in the painting, the order of prescedence or hierarchy is clearly defined. You can see what elements in the landscape are at the front, the middle and the back and you know that this hierarchy will never change.

Now notice how in my song above, the order of hierarchy of the different elements, tends to frequently change. My melodic music, if ever I make any, tends to do that.

# Fragmented Factions

[The Fun Thing (scrapped WIP)](https://soundcloud.com/tynamite/the-fun-thing-scrapped/s-5cq9rYuYFof)

Notice how the melody seems to be split into different factions, like how any other group like a social circle or political party can be split into multiple factions, whilst having enough similiarities to be part of the same group and not the other group.

For example the Conservative Party in the UK is split into 3 factions

* One Nation Conservative
* Better Together Conservative
* Big Society Conservative

So whilst they have enough similarities to not be a member of the opposition political party, they still have enough differences amongst themselves, to be split into factions.

Well that's how the melody in my song seems to behave like. And I don't even like the song anyway, I was just practising. It's supposed to sound like a 10 year old made it on garageband.

# Drone Music

This example will soon be with an representative example, once I manage to get this remix finished. How unfortunate! Right now it's a WIP.

 I can't really describe a music style, if I don't have a song to show you, can I? Well the downtempo emotional electronica Tynamite style, is to have 8-12 minute songs at a slow 80-110 BPM tempo, it be a slow riser and a slow creeper so it takes a long time to get to the point. The intros and outros are done in such a minimal and long way, that it's hard to tell when a song begins or ends. It's like a patchwork quilt how it creeps into a different territory while repeating the same thing for 2 minutes but at a slow enough speed and low energy understated away to avoid being gratingly annoying.

[Max Cooper - A Model of Reality (WIP)](https://soundcloud.com/tynamite/max-cooper-a-model-of-reality-tynamite-remixwip-crappy/s-Chh1DmrTMYx)

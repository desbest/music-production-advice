# Notational techniques for repetitive music

When it comes to repetitive music, I typically only like the song if it's doing either
1) It has a dub element or influence in it
2) There is some sort of melodic NOTATIONAL convention being used, merely just repeating the same notes, isn't enough

[Edwin Oosterwal - Get On](https://www.youtube.com/watch?v=cI6ijUPq05s)

[Mulder - Elon (Edwin Oosterwal Dub)](https://www.youtube.com/watch?v=3vaku0yOuUg)

[Cymbal Rush (The Field Remix)](https://youtu.be/Ow18a-CkLN4)

## Call and response

This song is a PERFECT example of "call and response" lyrics.
There is a call.....then a pause....then a response

```
Where'd you get your body from?
.......
I got it from my mama
```

Even the backing track has a call and response
The 1st and 2nd bar is VERY different from the 3rd and 4th bar, all THROUGHOUT the song

If you listen to the lyrics carefully, you'll notice that the ENTIRE song uses call and response lyrics

Now listen to these repetitive songs

[deadmau5 - 1891](https://www.youtube.com/watch?v=WuozphMw5Vs)

[Markus Homm - Violent Movement](https://www.youtube.com/watch?v=PZuOejFNElc)

[Basti Pieper - I Love You](https://www.youtube.com/watch?v=yNlMG3UBb8A)


## Chord progressions

### First Example

[Devilman - Sunny Days (Instrumental)](https://www.youtube.com/watch?v=CDT4REUaleM)

[Devilman - Freestyle @ Wunsen Media](https://www.youtube.com/watch?v=IPVUN3MkxBc)

In the Sunny Days song, the chord progression goes
```
E G C D
E G C D
```
Then it changes to
```
A F D E
A F D E
```
or maybe even
```
E G C D
A F D E 
F E A G
E D G F
```

That Mind Craft 360 instrumental 
```
F-5 E-5 B-4 G-5
```
Then the chord progression changes to
```
A-5 G-5 D-4 B-5
```

![Examples of octaves](https://i.imgur.io/wwkvGdO_d.webp?maxwidth=640&shape=thumb&fidelity=medium)

The 5th and 4th octave

And that electronic spring sound on the 4th bar is doing a different chord progression

The 5th octave is the one in the middle that everyone starts out with

### Freemasons example

Now for the Freemasons song
```
E F E D
F G F E
```
Then in the chorus it sometimes moves upwards towards the next ocatve
```
B-6 C-6 F-5 E-5
A-5 B-6 F-5 E-5
```

## Overlapping elements with varying pace

Here's another notational convention for repetition

Now for this example, they have THREE elements which are overlapping, where
1) one of them moves at a RELATIVELY slow pace
2) another one moves at a RELATIVELY fast pace
3) and the final one moves RELATIVELY at an intermediate pace, inbetween both of them

So there are actually THREE (or more) melodic elements, playing at once, just at a different PACE

PS. Pace is different to key and timing

[Pitto - Shadows (Edwin Oosterwal Remix](https://www.youtube.com/watch?v=fHDlnDq3_44) 

[Joris Voorn - Dark (Extended Mix)](https://www.youtube.com/watch?v=L379wLfcMOQ)

[Audiocrunch - Go To School Kid](https://www.youtube.com/watch?v=tUJvxdKKe7k)

## Still sounding good if you take out the drums

Imagine repetitive music, where if you was to REMOVE ALL the drums, it would STILL sound good

### Faithless example 

[Faithless - Don’t Leave (Euphoric Mix)](https://youtu.be/7IXASZPylE8)

[Faithless - God Is A Beckham](https://youtu.be/Vi03sLDh4KI)

[Faithless - I Want More (Extended Main Mix)](https://youtu.be/U16EqLNoWv0)

### Some other musician 

[Fedde Le Grand - The Creeps (Extended Mix)](https://youtu.be/TIMlMwbOMps)

[Dave Spoon - Bad Girl (at night) (Instrumental Mix)](https://youtu.be/e7dGHkJSJvI)

[Bona Fide - The Last Orchestra](https://www.youtube.com/watch?v=IdibGiaqD1o)

[Bona Fide - The Last Orchestra (Ambient Version)](https://www.youtube.com/watch?v=cPJQl0zT9Is)

[James Holden - A Break In The Clouds](https://www.youtube.com/watch?v=b5zzS40-xbo)

[James Holden - A Break In The Clouds (Ambient Version)](https://www.youtube.com/watch?v=zWp8iqzx0fI)

## Power chords

A power chord is a chord with 4 or more notes in
A typical chord is a major or minor chord which has 3 notes in it, where there's a space inbetween each note

```
C major is C-E-G
D minor is D-F-A
E major is E-G-A
F major is F-A-C
G major is G-B-D
A minor is A-C-E
B major is B-D-F
```
A power chord would go further by adding another note
Maybe it would be

```
C-E-G-B
D-F-A-C
```
But there's only so far a hand can stretch in order to reach the notes on a guitar, unless you're using some sort of weird guitar tuning, or having different strings on your guitar being tuned in a different key

With a power chord, a person would be having a chord of 4 notes at the bottom end of the keyboard, while playing a riff or melody on the top end of the keyboard.
However as the guitar is a string instrument, HOW you play a note on one end of the guitar, will affect all the other notes of the guitar, due to the strings vibrating.
If you play even ONE note on ONE string, it can affect how the OTHER strings sound, even if the strings AREN'T touching each other.

So with a power chord, which is the REASON why it's called a power chord, is because if you are playing a normal chord of 3 notes (as most music does), then it doesn't really affect the other end of the guitar much.

But if you're playing a power chord, the transition from 3 to 4 notes, SIGNIFICANTLY changes how the notes at the other end of the guitar, sound.

The melody or riff being played, ends up sounding muddied up or distorted.
This sound is VERY hard to recreate on a computer, I don't think computers can recreate that sound. It can only be done on string instruments.

With a power chord, a person would be having a chord of 4 notes at the bottom end of the keyboard, while playing a riff or melody on the top end of the keyboard.

However as the guitar is a string instrument, HOW you play a note on one end of the guitar, will affect all the other notes of the guitar, due to the strings vibrating.
If you play even ONE note on ONE string, it can affect how the OTHER strings sound, even if the strings AREN'T touching each other.

So with a power chord, which is the REASON why it's called a power chord, is because if you are playing a normal chord of 3 notes (as most music does), then it doesn't really affect the other end of the guitar much.

But if you're playing a power chord, the transition from 3 to 4 notes, SIGNIFICANTLY changes how the notes at the other end of the guitar, sound.

The melody or riff being played, ends up sounding muddied up or distorted.
This sound is VERY hard to recreate on a computer, I don't think computers can recreate that sound. It can only be done on string instruments.

That's why it's called a power chord. Because the transition from 3 to 4 notes within a chord, significantly changes how the notes for the riff or melody sound, from the other end of the guitar.

Some rock bands LOVE the power chords where EVERY song has to have power chords and EVERY song has to have the melody and riff being muddied up by the power chords. They love that effect.
And some rock bands have it more subtle, so it's not as noticeable, unless you're specifically choosing to be analysing the music or looking out for it.

[Johannes Neil - Consciousness ](https://www.youtube.com/watch?v=q9TQ-J3etS4)

[Britney Spears - Breathe on Me (James Holden Vocal Remix)](https://www.youtube.com/watch?v=rlB3ybJbMUI)

[Geyster - Under The Fuse of Love (Lifelike Remix)](https://www.youtube.com/watch?v=OSHXk_XR72E)

[Pig and Dan - Tears of a Clown (Max Cooper Remix)](https://www.youtube.com/watch?v=AxrMErNTCTQ)

[Eric Prydz - Call On Me](https://www.youtube.com/watch?v=qetW6R9Jxs4)

[Deepest Blue - Give It Away (Club Mix)](https://www.youtube.com/watch?v=LfpZf9GAyL0)

Those songs, they DO have power chords in them, it's just done in a subtle way. They don't want it to be obvious. They don't want you to notice it. It's kinda subliminal and inconspicuous.

If you listen carefully, you'll hear that the notes are kinda muddied up. They don't quite strike the same the second time it's playing the same note, than it does the first time. But the "intensive sound quality"  of the note wasn't reduced, they never really lowered the intensity of the sound in any way.

Now that I think about it, I don't believe that the musical notes have been pencilled in with a mouse, where someone clicks the notes in. I think that they've gotten a guitar to play it.

When they were making the song, they had the notes pencilled in at first, then right before they finished it, they got a guitar to play the synthesizer sounds, in order to produce that power chord effect, where the power chord muddies up the sound of the other notes being played.

Now that I think about it, I don't believe that the musical notes have been pencilled in with a mouse, where someone clicks the notes in. I think that they've gotten a guitar to play it.

When they were making the song, they had the notes pencilled in at first, then right before they finished it, they got a guitar to play the synthesizer sounds, in order to produce that power chord effect, where the power chord muddies up the sound of the other notes being played.

But it's VERY subtle, it's not supposed to be blatantly obvious.
Why would they do this?
I think they've done it, in order to break up the repetition, to make the repetitive music more tolerable and less annoying.

So that's another notational convention for repetitive music

## Matching harmoniously with a melodic vocal rock song, if both songs are slowed down

I would need to use my own music for this one. I don't really think anyone else does this, that I could possibly find. And it was completely unintentional. Some foreigner told me. I'll do it once my laptop is fixed.

![My laptop is being repaired](https://i.imgur.io/ErmRZL2_d.webp?maxwidth=640&shape=thumb&fidelity=medium)

## Anything else? Got any other ideas?

You tell me ;)

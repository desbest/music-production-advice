#  My general disposition 

## Environmentally aware sensitive fluctuating mood

My mood tends to fluctuate all the time every day as I have mood swings. There is a general mood that lasts 5-7 hours and within that, there's more tiny mood fluctuations that last somewhere between 30 minutes to 2 hours.

Not only that but it also fluctuates for every
* Place
* Person
* Activity
* Time

If I was at school in the classroom and I moved from one table that had my friend by it, to then move to another table to sit by my other friend, my emotion would instantly change. Also if I was at the university campus and I was to move from walking to one university building from another, from within 50 metres, my mood would also instantly change after arriving inside the building.

Most of the time I have a low mood, a melancholy mood, feeling the emotion of emptiness, well it's not exactly sad as it's too mild to be sad. It should be of no surprise, that I don't really have any happy music in my music library. All due to mild depression or dysthymia.

Also when it comes to dance music, for me it's progression over drops and low energy over high energy.

## Solemn or serene but never chirpy, for the always melancholy empty person

If you could categorise the emotion of music, into the following

1. Ghastly
2. Melancholy
3. Scenic
4. Chirpy

I would say that my music is either going to be 2 or 3.

And if downtempo was either
1. Serene
2. Solemn

What image would I use to represent serene or scenic?

![Scenic image](https://i.imgur.io/10sz3T0_d.webp?maxwidth=640&shape=thumb&fidelity=medium)

## Social setting is when on-the-go travelling

I wouldn't say it was intentional but now that I think about it, if I'm not making dance music for the nightclub, then I definitely make music to be listened to while you're travelling outside. 

Sure you could listen to it at home while lounging around (or in a coffee shop) but would anyone listen to Aphex Twin, Floating Points or Michael Jackson while travelling?

If not travelling music, then for open-air events like beaches, rooftop parties, 1am after-party, exhibitions, conferences, or drinking alcohol in the garden with your friends before getting ready to leave the house for an event

![Open air events 1](https://i.imgur.io/qk2a7lA_d.webp?maxwidth=640&shape=thumb&fidelity=medium)

![Open air events 2](https://i.imgur.io/pZonVo2_d.webp?maxwidth=640&shape=thumb&fidelity=medium)

## My analogy about stepping in puddles to diffract car headlights

Imagine that someone is sat on a wooden bench outside, at the edge of it, while you are standing on the opposite end of the bench, jumping up and down. Even though the person is sat on a solid object, it can't be 100% sturdy as you jumping on it, causes the person to be moving up and down.

* **You're in the room but not a part of the room**
* There is a level of uncertainty there. **The uncertainty of life.** Nothing is stable and nothing is ever not increasingly complex.**
* **There is a fragility of life.** Tomorrow isn't promised today and ALSO what you have right now can be easily taken away right now in an instant.
* **Dysthymia induced emptiness**

Now imagine that it's dark outside and you're walking and there's some puddles on the pavement and lots of cars driving down the road. All the cars have their headlights on in front of them. **Imagine that when you step in the puddles, that it alters the direction of the headlights.** That's exactly the emotion that is prevalent in all my songs. **Also that, you're inside the room but not a part of the room. And that you're levitating a few centimetres above the ground.** Well that as well.

## My 2nd analogy about observing your city from a bus shelter whilst holding a camera

I think me being against consumerism, it being a complete anathema to me, has somehow had an emotion in my music.

I don't have much possessions, all my possessions can fit in a 4 seater car with folding back seats. I don't really care about going on holiday.

I follow the philosophy of, to quote Sister Bliss, that if you’re bored of London then you’re bored of the entirety of the human experience itself, as if you can’t find a sense of fun, harmony and belonging in the urban jungle and patchwork quilt of London, then no amount of travelling abroad can ever appease or placate you. I have all the experiences I’ll ever need in my city of Birmingham with its 1.2 million population. There’s a lot of underground scenes and gated communities here.

So imagine that you're sat inside a bus shelter, which then shields away the very top and the very bottom of the landscape. The way I see the common recurring emotion in my music, it's not cultural voyeurism to take someone on holiday for something completely different than what they're used to but instead to ask myself, **what would be something to want to walk outside the bus shelter, to get a closer look, for a city I've lived in my whole life?** Maybe there's a rainbow, a weird antique car or an ice cream van.

## Representative Photos
I like things that are dark and dingy.

![Dark and dingy photo 1](https://i.imgur.io/qj5JW60_d.webp?maxwidth=640&shape=thumb&fidelity=medium)

![Dark and dingy photo 2](https://i.imgur.io/XBOkYkQ_d.webp?maxwidth=640&shape=thumb&fidelity=medium)

![Dark and dingy photo 3](https://i.imgur.io/qPhuNkR_d.webp?maxwidth=640&shape=thumb&fidelity=medium)

![Dark and dingy photo 4](https://i.imgur.io/GMUN5nT_d.webp?maxwidth=640&shape=thumb&fidelity=medium)

![Dark and dingy photo 5](https://i.imgur.io/9myuHxY_d.webp?maxwidth=640&shape=thumb&fidelity=medium)

![dark and dingy photo 6](https://i.imgur.io/DTFtbfq_d.webp?maxwidth=640&shape=thumb&fidelity=medium)

## Dusk coloured skies

I also like skies that are dark and dingy, like the interior design of Costa Coffee.

![Costa coffee, interior design 1](https://i.imgur.io/lKh5xnR_d.webp?maxwidth=640&shape=thumb&fidelity=medium)

![Costa coffee, interior design 2](https://i.imgur.io/EuboyVL_d.webp?maxwidth=640&shape=thumb&fidelity=medium)

_add images later of different skies, once my laptop is repaired_

* Purple
* Dirty dark lilac
* Acidic orange
* Dirty dark navy (not Royal Navy)
* Citric muddy pink (similar to cough syrup)
* Dark green
